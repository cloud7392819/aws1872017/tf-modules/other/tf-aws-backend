# ws@2023 locals.tf

##################################################################
# Common/General settings
##################################################################

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false

  name = var.name

  region = var.region
    
  # S3 for tfstate
  tf_state_s3_bucket = "${var.name}-tf-remote-state-all-${var.region}"

  # DynamoDB for tfstate
  tf_state_dynamodb_table = "${var.name}-tf-remote-state-locks-${var.region}"


  tags = merge(
    var.module_tags,
    {
      Terraform = true
    }
  )
}


