![Terraform](https://lgallardo.com/images/terraform.jpg)

# tf-aws-backend
---
This module manage AWS .. [terraform-module-aws-backend].

## MODULE

```hcl
module "backend" {

  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/other/tf-aws-backend.git?ref=tags/1.0.x"

  ##################################################################
  # Common/General settings
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = false

  region = var.region

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Application name
  name   = var.cut_name

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    ManagedBy   = "Terraform"
  }
}
```
To run this example you need to execute:
```bash
$ terraform init
```

# Create
## Uncomment terraform {}

```hcl
terraform {
   
  backend "s3" {    
    
    #"${var.name}-tf-remote-state-all-${var.region}"
    bucket                    = "<name>-tf-remote-state-all-<region>"
    #"${var.region}"
    key                       = "AWS/<region>/backend/terraform.tfstate"
    #"${var.region}"
    region                    = "<region>"
    #"${var.name}-tf-remote-state-locks-${var.region}"
    dynamodb_table            = "<name>-tf-remote-state-locks-<region>"
    encrypt                   = true

  }
}

To run this example you need to execute:
```bash
$ terraform init

Initializing the backend...
Acquiring state lock. This may take a few moments...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "s3" backend. No existing state was found in the newly
  configured "s3" backend. Do you want to copy this state to the new "s3"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: yes
```

```
## Usage

To run this example you need to execute:
```bash
$ terraform plan
$ terraform apply
```

# DELETE
## Comment terraform {}

To run this example you need to execute:
```bash
$ terraform init -migrate-state

Initializing the backend...
Terraform has detected you're unconfiguring your previously set "s3" backend.
Acquiring state lock. This may take a few moments...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "s3" backend to the
  newly configured "local" backend. No existing state was found in the newly
  configured "local" backend. Do you want to copy this state to the new "local"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: yes

Releasing state lock. This may take a few moments...

Successfully unset the backend "s3". Terraform will now operate locally.
Initializing modules...

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Using previously-installed hashicorp/aws v5.30.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

$ terraform plan
```



<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 2.1 |

## Providers

No providers.

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 2.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_xxx"></a> [cache\_bucket](#module\_cache\_bucket) | git::https://gitlab.com/xxxx | 6.0.0 |


## Resources

No resources.

| Name | Type |
|------|------|

| [aws_iam_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.default_cache_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="module_enable"></a> [module\_enable](#input\_module\_enable) | Whether to create the resources | `bool` | `false` | true |
| <a name="module_depend_on"></a> [module\_depend\_on](#input\_module\_depend\_on) | Emulation of behavior for the module. | `string` | `""` | no |
| <a name="name"></a> [name](#input\_name) | Application name | `string` | `""` | name |
| <a name="module_tags"></a> [module\_tags](#input\_module\_tags) | Additional mapping of tags to assign to the all linked resources | `map(string)` | `{}` | no |

## Outputs

No outputs.

| Name | Description |
|------|-------------|
| <a name="output_project_name"></a> [project\_name](#output\_project\_name) | Project name |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | IAM Role ARN |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | IAM Role ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.

## URLs

1.
2.


##