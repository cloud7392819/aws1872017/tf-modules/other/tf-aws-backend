# ws@2023 outputs.tf

##################################################################
# Common/General settings
##################################################################

##################################################################
# S3 Bucket settings
##################################################################

output "s3_bucket_id" {
  description = "The name of the S3 bucket"  
  value       = concat(aws_s3_bucket.terraform_state.*.id, [""])[0]
}

output "s3_bucket_domain_name" {
  description = "The name of the S3 bucket"  
  value       = concat(aws_s3_bucket.terraform_state.*.bucket_domain_name, [""])[0]
}

##################################################################
# DynamoDB settings
##################################################################

output "dynamodb_table_name" {
  description = "The name of the DynamoDB table"
  value       = concat(aws_dynamodb_table.terraform_state_locks.*.name, [""])[0]
}


