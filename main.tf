# ws@2023 main.tf

##################################################################
# Common/General settings
##################################################################


##################################################################
# Create a dynamodb table for locking the state file
##################################################################

resource "aws_dynamodb_table" "terraform_state_locks" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0    

  name         = local.tf_state_dynamodb_table
  
  billing_mode = "PAY_PER_REQUEST"
  
  hash_key     = "LockID"
  
  attribute {
    name = "LockID"
    type = "S"
  }
  
  tags = merge(
    local.tags,
    {
      "Name"        = local.tf_state_dynamodb_table
      "Description" = "DynamoDB terraform table to lock states"
    }
  )
}

##################################################################
# Create an S3 bucket to store the state file in
##################################################################

resource "aws_s3_bucket" "terraform_state" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  
  
  bucket = local.tf_state_s3_bucket
  
  lifecycle {
    prevent_destroy = true
  }
  
  tags = merge(
    local.tags,
    {
      Name        = local.tf_state_s3_bucket
      Description = "S3 Remote Terraform State Store"
    }
  )
}

#
resource "aws_s3_bucket_public_access_block" "s3_access_block" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  bucket                  = aws_s3_bucket.terraform_state[count.index].id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}



