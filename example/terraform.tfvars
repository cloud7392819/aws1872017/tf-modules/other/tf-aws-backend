# ws@2023 terraform.tf

##################################################################
# TFVARS 
##################################################################

# Region
region                  = "us-east-1"

# Application
name                    = "name"
cut_name                = "name"

# Environment [info]
environment             = "env"
ownerEmail              = "name@mail.com"
team                    = "DevOps"
deployedby              = "Terraform"

